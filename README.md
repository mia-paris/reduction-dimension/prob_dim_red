# Install

```bash
pip install prob-dim-red --index-url https://forgemia.inra.fr/api/v4/projects/10272/packages/pypi/simple
```

# Contribute

- clone the repo
  ```bash
  git clone THIS_REPO
  ```

- local dev install
  ```bash
  pip install .[dev]
  ```

- install pre-commit
  ```bash
  pre-commit install
  ```
