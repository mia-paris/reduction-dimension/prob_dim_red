.. prob_dim_red documentation master file, created by
   sphinx-quickstart on Wed Feb 14 15:10:08 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to prob_dim_red's documentation!
========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ./linear_gaussian_ppca.rst
   ./cli.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
