Linear Gaussian pPCA documentation
==================================

.. automodule:: prob_dim_red.linear_gaussian_ppca
   :members:
   :imported-members:
   :inherited-members:
   :special-members: __init__
   :undoc-members:
   :show-inheritance:
